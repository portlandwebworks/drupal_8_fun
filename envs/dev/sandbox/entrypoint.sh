#!/bin/bash

# This is required because the env vars passed in docker-compose.yml will only
# be in effect during compose. Any other ssh sessions (like one initiated by
# Drush) won't get the vars. This also cannot be done in the Dockerfile as
# env vars are provided on runtime rather than on build.
env >> /etc/environment

# Copy the template settings to the mounted Drupal. This is because we are
# mounting the entire drupal directory and anything prepared in the Dockerfile
# is shadowed over, like the mount command.
cp /root/drupal/* /var/www/html/sites/default

# Finally, run everything
exec supervisord -n