<?php

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}

$aliases['local'] = [
    'remote-host' => 'local.vermont.com',
    'remote-user' => 'root',
    'ssh-options' => '-p 2222',
    'db-url' => 'mysql://root:rootpassword@vermont-mysql/vermont',
    'root' => '/var/www/html',
    'uri' => 'local.vermont.com',
    'path-aliases' => [
        '%dump-dir' => "/tmp/backup",
        '%dump' => '/tmp/backup/local.sql'
    ],
];