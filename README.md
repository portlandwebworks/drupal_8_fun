# Drupal 8 Playground

## Purpose

This project will setup a Docker-based Drupal 8 environment that you can use to immediately start vetting some of the new features of Drupal 8. At the end of the process, you will have:

1. Three Docker instances
	1. Apache server
	1. Mysql database
	1. An administrative instance
1. A running drupal site that you can navigate to

## Requirements

1. The Dependency manager, [PHP Composer](https://getcomposer.org)
1. [Docker Beta](https://blog.docker.com/2016/03/docker-for-mac-windows-beta/)
1. [Drush](http://www.drush.org/en/master/)
1. Your host machine is a flavor of *nix


## Run Steps

1. **Really Important**: Stop any running docker images. It'll save you a lot of troubleshooting pain
1. from the directory where this README is, execute

	```bash

	./setup

	```
	
1. Provide your computer's sudo password. _(this is used to add a host name to the `/etc/hosts` file)_

The url to the new site plus the admin user's name and password will be displayed once the setup process finishes.


## Troubleshooting

1. If an error message comes up stating port `3306` is in use, that indicates you have another instance of mySql listening on that port. Turn off all docker images and any mysql server you may be running on the host box itself.


## Items to Vet

1. Solr integration with D8 search api — is in ‘alpha’ at present—do we even need solr?
1. Search documents (tika)
1. weighted searches
1. Make sure that D8 web forms are usable
1. Is there behaviour driven functionality for identifying a type of user even when they're anonymous?
1. PDF generation from html
1. Location-based modules. How mature are they?
1. can google translate translate specific pages, and is the translated content searchable?
1. Social media integration
1. How to separate content from config. Come up with a process
1. Define the difference between workflow and workbench. Test to see which is working in D8






