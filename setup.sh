#!/bin/bash

function echocolor() { # $1 = string
    COLOR='\033[1;33m'
    NC='\033[0m'
    printf "${COLOR}$1${NC}\n"
}

PROJECT_NAME=vermont
DRUPAL_ADMIN=admin
DRUPAL_PASSWORD=password
DATABASE_PASSWORD=${DRUPAL_PASSWORD}
DATABASE_USER=user

PROJECT_URL="local.${PROJECT_NAME}.com"
DOCKER_COMPOSER_FILE='./envs/dev/docker-compose.yml';
DRUSH_ALIAS_TARGET=~/.drush/${PROJECT_NAME}.aliases.drushrc.php

echocolor "Adding ${PROJECT_NAME} drush alias file"
cp ./envs/dev/${PROJECT_NAME}.aliases.drushrc.php $DRUSH_ALIAS_TARGET;
sed -i '' "s/\*\*PROJECT_NAME\*\*/${PROJECT_NAME}/g" $DRUSH_ALIAS_TARGET;


echocolor "Enter your sudo password to update your hosts file."
echo "127.0.0.1 ${PROJECT_URL}" | sudo tee -a /etc/hosts > /dev/null
if [[ $? -ne 0 ]]; then
	exit 1;
fi

echocolor "configuring ssh"

echocolor "copying public key for docker instances"
SANDBOX_SSH_DIR=./envs/dev/sandbox/ssh

echocolor "adding project ssh key to your ssh directory"
cp $SANDBOX_SSH_DIR/id_rsa_$PROJECT_NAME ~/.ssh/;
if [[ $? -ne 0 ]]; then
	exit 1;
fi
chmod 600 ~/.ssh/id_rsa_$PROJECT_NAME;
if [[ $? -ne 0 ]]; then
	exit 1;
fi

echocolor "adding drush ssh config settings."
cat >> ~/.ssh/config << EOL

Host ${PROJECT_URL}
    identityfile ~/.ssh/id_rsa_$PROJECT_NAME
    StrictHostKeyChecking no
    port 2222
    user root

EOL
if [[ $? -ne 0 ]]; then
	exit 1;
fi

echocolor "installing 3rd party dependencies"
#Assume we have php Composer installed
composer -vv install;
if [[ $? -ne 0 ]]; then
	exit 1;
fi

echocolor "Starting Docker containers. This could take a bit"
#Assume docker-beta's been installed
docker-compose -f $DOCKER_COMPOSER_FILE up -d;
if [[ $? -ne 0 ]]; then
	exit 1;
fi

echocolor "Initializing Drush database"

#TODO: there's a problem where drush will install, but sends an error message anyway.
# Don't keep trying to run the installer more than once, hence advancing the counter to 4 -jg
COUNTER=4

#Modify the permissions on the file. Drupal makes it read only -jg
chmod uog+rw web/sites/default/*

while [  $COUNTER -lt 5 ]; do
	sleep 10 #set it to 10 for super slow mysql startups. This is related to the above TODO. -jg
	echocolor "attempt $COUNTER of 5 to configure the drush database"
	

	let COUNTER=COUNTER+1 
	#Assume drush is installed. This also applies configuration files
	drush -y --verbose --debug @${PROJECT_NAME}.local site-install config_installer \
	--db-url=mysql://${DATABASE_USER}:${DATABASE_PASSWORD}@${PROJECT_NAME}-mysql/${PROJECT_NAME} \
	--account-name=${DRUPAL_ADMIN} \
	--account-pass=${DRUPAL_PASSWORD} \
	--site-name=${PROJECT_NAME} \
	--config-dir=sites/default/sync
	
	# result=$?

	# if [ $result -ne 0 ] && [ $COUNTER -eq 5 ]; then
	# 	exit 1;
	# elif [ $result -ne 0 ] && [ $COUNTER -ne 5 ]; then
	# 	continue;
	# else
	# 	break;
	# fi
done



echocolor "install complete. Presently, the install will return an error even though the install was successful."
echocolor "Read the output of the log to make sure your install worked"
echocolor "Assuming all went well, visit your new site at http://${PROJECT_URL}. Admin creds are ${DRUPAL_ADMIN}/${DRUPAL_PASSWORD}"

exit 0;