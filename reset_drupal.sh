#!/usr/bin/env bash

PROJECT_NAME=vermont
DATABASE_USER=user
DATABASE_PASSWORD=password
DRUPAL_ADMIN=admin
DRUPAL_PASSWORD=password

chmod uog+rw ./web/sites/default/*

drush -y --verbose --debug @${PROJECT_NAME}.local site-install config_installer \
	--db-url=mysql://${DATABASE_USER}:${DATABASE_PASSWORD}@${PROJECT_NAME}-mysql/${PROJECT_NAME} \
	--account-name=${DRUPAL_ADMIN} \
	--account-pass=${DRUPAL_PASSWORD} \
	--site-name=${PROJECT_NAME} \
	--config-dir=sites/default/sync